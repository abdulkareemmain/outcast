<?php

namespace App\Http\Controllers;

use App\BankDetail;
use App\Employee;
use App\Role;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{


    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (auth()->user()->role_id != 1) {
                return redirect()->back();
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(25);
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'address' => 'required',
            'contact' => 'required|numeric|digits:11',
            'first_name' => 'required',
            'last_name' => 'required',
            'cnic_address' => 'required',
            'email' => 'unique:users,email',
            'cnic' => 'required|numeric|digits:13',
            'account_number' => 'numeric|digits_between:14,16',
            'emergency_name' => 'required',
            'emergency_relationship' => 'required',
            'emergency_contact' => 'required|numeric|digits:11',
            'profile_pic' => 'required|max:6000|mimes:img,jpg,jpeg,png',
            'cnic_pic' => 'required|max:6000|mimes:img,jpg,jpeg,png',
            'salary_slip_pic' => 'max:6000|mimes:img,jpg,jpeg,png',
        ]);

        $salary_slip_pic = null;
        $profile_pic = $request->file('profile_pic');
        $profile_pic = $this->uploadFile($profile_pic, 'documents');

        $cnic_pic = $request->file('cnic_pic');
        $cnic_pic = $this->uploadFile($cnic_pic, 'documents');

        if ($request->hasFile('salary_slip_pic')) {
            $salary_slip_pic = $request->file('salary_slip_pic');
            $salary_slip_pic = $this->uploadFile($salary_slip_pic, 'documents');
        }

        \DB::transaction(function () use ($salary_slip_pic, $profile_pic, $cnic_pic, $request) {
            $user = User::create($request->except('profile_pic', 'password') + ['profile_pic' => $profile_pic, 'password' => Hash::make($request->password)]);

            Employee::create($request->except('user_id', 'cnic_pic', 'salary_slip_pic') + ['user_id' => $user->id, 'cnic_pic' => $cnic_pic, 'salary_slip_pic' => $salary_slip_pic]);
            BankDetail::create($request->except('user_id') + ['user_id' => $user->id]);
        });
        return redirect()->back()->with(['message' => 'created']);
    }

    public function uploadFile($file, $path)
    {
        $destinationPath = $path;
        $originalFile = $file->getClientOriginalName();
        $filename1 = time() . '-' . $originalFile;
        $filename2 = $filename1;
        $fileextension = $file->getClientOriginalExtension();
        $filesize = $file->getSize();
        $file->move($destinationPath, $filename1);
        return $filename2;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();
        $user = User::find($id);
        return view('admin.users.edit', compact('roles', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (auth()->user()->role_id != 1) {
            return redirect()->back();
        }
        $this->validate($request, [
            'address' => 'required',
            'contact' => 'required|numeric|digits:11',
            'first_name' => 'required',
            'last_name' => 'required',

            'cnic_address' => 'required',
            'email' => 'required',
            'cnic' => 'required|numeric|digits:13',
            'account_number' => 'required|numeric|digits_between:14,16',

            'emergency_name' => 'required',
            'emergency_relationship' => 'required',
            'emergency_contact' => 'required|numeric|digits:11',
            'profile_pic' => 'max:6000|mimes:img,jpg,jpeg,png',
            'cnic_pic' => 'max:6000|mimes:img,jpg,jpeg,png',
            'salary_slip_pic' => 'max:6000|mimes:img,jpg,jpeg,png',
        ]);

        $user = User::find($id);
        $salary_slip_pic = $user->employee->salary_slip_pic;
        $profile_pic = $user->profile_pic;
        $cnic_pic = $user->employee->cnic_pic;

        if ($request->hasFile('profile_pic')) {
            $profile_pic = $request->file('profile_pic');
            $profile_pic = $this->uploadFile($profile_pic, 'documents');
        }

        if ($request->hasFile('cnic_pic')) {
            $cnic_pic = $request->file('cnic_pic');
            $cnic_pic = $this->uploadFile($cnic_pic, 'documents');
        }

        if ($request->hasFile('salary_slip_pic')) {
            $salary_slip_pic = $request->file('salary_slip_pic');
            $salary_slip_pic = $this->uploadFile($salary_slip_pic, 'documents');
        }

        $employee = Employee::where('user_id', $id)->first();
        $bank_detail = BankDetail::where('user_id', $id)->first();

        \DB::transaction(function () use ($salary_slip_pic, $profile_pic, $cnic_pic, $request, $employee, $bank_detail, $user, $id) {

            if ($request->password != null) {
                $user->update($request->except('_token', '_method', 'profile_pic', 'password') + ['profile_pic' => $profile_pic, 'password' => Hash::make($request->password)]);
            } else {
                $user->update($request->except('_token', '_method', 'profile_pic', 'password') + ['profile_pic' => $profile_pic]);
            }
            Employee::find($employee->id)->update($request->except('_token', '_method', 'user_id', 'cnic_pic', 'salary_slip_pic') + ['user_id' => $id, 'cnic_pic' => $cnic_pic, 'salary_slip_pic' => $salary_slip_pic]);
            BankDetail::find($bank_detail->id)->update($request->except('_token', '_method', 'user_id') + ['user_id' => $id]);
        });
        return redirect()->back()->with(['message' => 'created']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id', $id)->delete();
        Employee::where('user_id', $id)->delete();
        BankDetail::where('user_id', $id)->delete();

        return redirect()->back();
    }
}
