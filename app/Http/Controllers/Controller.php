<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function uploadFile($file, $path)
    {
        $destinationPath = $path;
        $originalFile = $file->getClientOriginalName();
        $filename1 = time() . '-' . $originalFile;
        $filename2 = $filename1;
        $fileextension = $file->getClientOriginalExtension();
        $filesize = $file->getSize();
        $file->move($destinationPath, $filename1);
        return $filename2;
    }
}
