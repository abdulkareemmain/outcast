<?php

namespace App\Http\Controllers;

use App\BankDetail;
use App\Employee;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->role_id != 1) {
            return redirect()->back();
        }
        $employees = Employee::paginate(25);
        return view('admin.employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user() != null) {
            if (auth()->user()->role->name == 'admin') {
                return view('admin.employees.create');
            }
        }
        return view('frontend.forms.registration');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'address' => 'required',
            'contact' => 'required|numeric|digits:11',
            'first_name' => 'required',
            'last_name' => 'required',
            'cnic_address' => 'required',
            'email' => 'unique:users,email',
            'cnic' => 'required|numeric|digits:13',
            'account_number' => 'nullable|numeric|digits_between:14,16',
            'emergency_name' => 'required',
            'emergency_relationship' => 'required',
            'emergency_contact' => 'required|numeric|digits:11',
            'profile_pic' => 'required|max:6000|mimes:img,jpg,jpeg,png',
            'cnic_pic' => 'required|max:6000|mimes:img,jpg,jpeg,png',
            'salary_slip_pic' => 'max:6000|mimes:img,jpg,jpeg,png',
        ]);

        $salary_slip_pic = null;
        $is_approved = 0;

        if (optional(auth()->user())->role_id == 1) {
            $is_approved = $request->is_approved;
        }
        $profile_pic = $request->file('profile_pic');
        $profile_pic = $this->uploadFile($profile_pic, 'documents');

        $cnic_pic = $request->file('cnic_pic');
        $cnic_pic = $this->uploadFile($cnic_pic, 'documents');

        if ($request->hasFile('salary_slip_pic')) {
            $salary_slip_pic = $request->file('salary_slip_pic');
            $salary_slip_pic = $this->uploadFile($salary_slip_pic, 'documents');
        }

        \DB::transaction(function () use ($salary_slip_pic, $profile_pic, $cnic_pic,$is_approved, $request) {
            $user = User::create($request->except('profile_pic', 'password', 'is_approved') + ['is_approved'=>$is_approved,'profile_pic' => $profile_pic, 'password' => Hash::make($request->password)]);
            Employee::create($request->except('user_id', 'cnic_pic', 'salary_slip_pic') + ['user_id' => $user->id, 'cnic_pic' => $cnic_pic, 'salary_slip_pic' => $salary_slip_pic]);
            BankDetail::create($request->except('user_id') + ['user_id' => $user->id]);
        });
        return redirect()->back()->with(['message' => 'Employee Created Successfully']);
    }

    public function uploadFile($file, $path)
    {
        $destinationPath = $path;
        $originalFile = $file->getClientOriginalName();
        $filename1 = time() . '-' . $originalFile;
        $filename2 = $filename1;
        $fileextension = $file->getClientOriginalExtension();
        $filesize = $file->getSize();
        $file->move($destinationPath, $filename1);
        return $filename2;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (auth()->user()->role_id != 1) {
            return redirect()->back();
        }
        $this->validate($request, [
            'address' => 'required',
            'contact' => 'required|numeric|digits:11',
            'first_name' => 'required',
            'last_name' => 'required',
            'cnic_address' => 'required',
            'email' => 'required',
            'cnic' => 'required|numeric|digits:13',
            'account_number' => 'nullable|numeric|digits_between:14,16',
            'emergency_name' => 'required',
            'emergency_relationship' => 'required',
            'emergency_contact' => 'required|numeric|digits:11',
            'profile_pic' => 'max:6000|mimes:img,jpg,jpeg,png',
            'cnic_pic' => 'max:6000|mimes:img,jpg,jpeg,png',
            'salary_slip_pic' => 'max:6000|mimes:img,jpg,jpeg,png',
        ]);

        $user = User::find($id);
        $salary_slip_pic = $user->employee->salary_slip_pic;
        $profile_pic = $user->profile_pic;
        $cnic_pic = $user->employee->cnic_pic;
        $is_approved = 0;

        if (optional(auth()->user())->role_id == 1) {
            $is_approved = $request->is_approved;
        }

        if ($request->hasFile('profile_pic')) {
            $profile_pic = $request->file('profile_pic');
            $profile_pic = $this->uploadFile($profile_pic, 'documents');
        }

        if ($request->hasFile('cnic_pic')) {
            $cnic_pic = $request->file('cnic_pic');
            $cnic_pic = $this->uploadFile($cnic_pic, 'documents');
        }

        if ($request->hasFile('salary_slip_pic')) {
            $salary_slip_pic = $request->file('salary_slip_pic');
            $salary_slip_pic = $this->uploadFile($salary_slip_pic, 'documents');
        }

        $employee = Employee::where('user_id', $id)->first();
        $bank_detail = BankDetail::where('user_id', $id)->first();

        \DB::transaction(function () use ($salary_slip_pic, $profile_pic, $cnic_pic, $request, $employee, $bank_detail, $user, $id, $is_approved) {

            if ($request->password != null) {
                $user->update($request->except('profile_pic', 'is_approved') + ['is_approved'=>$is_approved,'profile_pic' => $profile_pic, 'password' => Hash::make($request->password)]);
            } else {
                $user->update($request->except('profile_pic', 'password') + ['profile_pic' => $profile_pic]);
            }
            Employee::find($employee->id)->update($request->except('user_id', 'cnic_pic', 'salary_slip_pic') + ['user_id' => $id, 'cnic_pic' => $cnic_pic, 'salary_slip_pic' => $salary_slip_pic]);
            BankDetail::find($bank_detail->id)->update($request->except('user_id') + ['user_id' => $id]);

        });
        return redirect()->back()->with(['message' => 'created']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->role_id != 1) {
            return redirect()->back();
        }
    }
}
