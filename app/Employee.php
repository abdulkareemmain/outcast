<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;
    protected $fillable=[
        'blood_group','cnic_address','cnic','allergies','emergency_name','emergency_relationship','emergency_contact','emergency_email','user_id','salary_slip_pic','cnic_pic'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
