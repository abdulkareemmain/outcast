<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankDetail extends Model
{

    use SoftDeletes;
    protected $fillable=[
        'account_title','account_number','bank_name','status','user_id'
    ];
}
