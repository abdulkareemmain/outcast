<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('blood_group')->nullable();
            $table->string('cnic', 14);
            $table->text('cnic_address');
            $table->string('allergies')->nullable();
            $table->string('emergency_name');
            $table->string('emergency_relationship');
            $table->string('emergency_contact', 11);
            $table->string('emergency_email')->nullable();
            $table->string('cnic_pic')->nullable();
            $table->string('salary_slip_pic')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
