@extends('layouts.app')
@section('content')
    <br>
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">User Information</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if ($errors->any())
                {!! implode('', $errors->all('<li class="text-danger">:message</li>')) !!}
            @endif
            <form method="post" action="{{ route('employees.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <!-- text input required -->
                    <div class="col-sm-6">

                        <div class="form-group">
                            <label>Role</label>
                            <select required name="role_id" id="" class="form-control">
                                <option value="" selected disabled>Select</option>
                                @foreach ($roles as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-6">

                        <div class="form-group">
                            <label>Approved</label>
                            <select required name="is_approved" id="" class="form-control">
                                <option value="" selected disabled>Select</option>
                                <option value="1">Approved</option>
                                <option value="0">Non-Approved</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <!-- text input required -->
                        <div class="form-group">
                            <label>First Name</label>
                            <input required name="first_name" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Last Name</label>
                            <input required name="last_name" type="text" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <!-- text input required -->
                        <div class="form-group">
                            <label>Email</label>
                            <input required name="email" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Password</label>
                            <input required name="password" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <!-- textarea -->
                        <div class="form-group">
                            <label>Contact</label>
                            <input required name="contact" type="text" pattern="\d*" minlength="11" maxlength="11" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <!-- textarea -->
                        <div class="form-group">
                            <label>Profile Picture</label>
                            <input required type="file" accept="image/*" class="form-control" name="profile_pic">
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Address</label>
                            <textarea name="address" class="form-control" rows="2"></textarea>
                        </div>
                    </div>



                </div>


        </div>
        <!-- /.card-body -->
    </div>

    {{-- --------------------------------------------------Employee Information------------------------------------------------------------------- --}}

    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Personal Details</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">

            <div class="row">
                <div class="col-sm-6">
                    <!-- text input required -->
                    <div class="form-group">
                        <label>Blood Group</label>
                        <input required name="blood_group" type="text" class="form-control">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>CNIC</label>
                        <input required name="cnic" type="text" pattern="\d*" minlength="13" maxlength="13" class="form-control">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <!-- text input required -->
                    <div class="form-group">
                        <label>allergies</label>
                        <input required name="allergies" type="text" class="form-control">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Emergency Name</label>
                        <input required name="emergency_name" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <!-- textarea -->
                    <div class="form-group">
                        <label>Emergency Contact</label>
                        <input required name="emergency_contact" type="text" pattern="\d*" minlength="11" maxlength="11" class="form-control">
                    </div>
                </div>

                <div class="col-sm-6">
                    <!-- textarea -->
                    <div class="form-group">
                        <label>Emergency Relationship</label>
                        <input required name="emergency_relationship" type="text" class="form-control">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>CNIC Address</label>
                        <textarea name="cnic_address" class="form-control" rows="2"></textarea>
                    </div>
                </div>

                {{-- <div class="col-sm-6">
                        <!-- textarea -->
                        <div class="form-group">
                            <label>CNIC Picture</label>
                            <input required type="file" accept="image/*" class="form-control" name="cnic_picture">
                        </div>
                    </div> --}}


            </div>

            <div class="row">

                <div class="col-sm-6">
                    <!-- textarea -->
                    <div class="form-group">
                        <label>Salary Slip Picture</label>
                        <input type="file" accept="image/*" class="form-control" name="salary_slip_pic">
                    </div>
                </div>

                <div class="col-sm-6">
                    <!-- textarea -->
                    <div class="form-group">
                        <label>CNIC Picture</label>
                        <input required type="file" accept="image/*" class="form-control" name="cnic_pic">
                    </div>
                </div>


            </div>


        </div>
        <!-- /.card-body -->
    </div>


    {{-- --------------------------------------------------Bank Information------------------------------------------------------------------- --}}

    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Bank Details</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">

            <div class="row">
                <div class="col-sm-6">
                    <!-- text input required -->
                    <div class="form-group">
                        <label>Bank Name</label>
                        <input required name="bank_name" type="text" class="form-control">
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-6">
                    <!-- text input required -->
                    <div class="form-group">
                        <label>Account Title</label>
                        <input required name="account_title" type="text" class="form-control">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Account Number</label>
                        <input required name="account_number" type="text" pattern="[0-9]{14,16}" class="form-control">
                    </div>
                </div>
            </div>


            <div class="text-right">
                <input required type="submit" value="Submit" class="btn btn-primary btn-lg">
            </div>
            </form>
        </div>
        <!-- /.card-body -->
    </div>

    @if (Session::has('message'))
        <script>
            $(document).ready(function() {
                Toast.fire({
                    icon: 'success',
                    title: 'User Created Successfully'
                })
            });
        </script>
    @endif

@endsection
