@extends('layouts.app')
@section('content')
    <br>
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Add User</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if ($errors->any())
                {!! implode('', $errors->all('<li class="text-danger">:message</li>')) !!}
            @endif
            <form method="post" action="{{ route('employees.update',$user->id) }}" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="row">
                    <!-- text input -->
                    <div class="col-sm-6">

                        <div class="form-group">
                            <label>Role</label>
                            <select required name="role_id" id="" class="form-control">
                                <option value="" selected disabled>Select</option>
                                @foreach ($roles as $item)
                                    <option value="{{ $item->id }}" @if($user->role_id==$item->id) selected @endif>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-6">

                        <div class="form-group">
                            <label>Approved</label>
                            <select required name="is_approved" id="" class="form-control">
                                <option value="" selected disabled>Select</option>
                                <option @if($user->is_approved) selected @endif value="1">Approved</option>
                                <option @if(!$user->is_approved) selected @endif value="0">Non-Approved</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <!-- text input -->
                        <div class="form-group">
                            <label>First Name</label>
                            <input name="first_name" type="text" class="form-control" value="{{$user->first_name}}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Last Name</label>
                            <input name="last_name" type="text" class="form-control" value="{{$user->last_name}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <!-- text input -->
                        <div class="form-group">
                            <label>Email</label>
                            <input name="email" type="text" class="form-control" value="{{$user->email}}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Password</label>
                            <input name="password" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <!-- textarea -->
                        <div class="form-group">
                            <label>Contact</label>
                            <input name="contact" type="text" pattern="\d*" minlength="11" maxlength="11" class="form-control" value="{{$user->contact}}">
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <!-- textarea -->
                        <div class="form-group">
                            <label>Profile Picture</label>
                            <input type="file" accept="image/*" class="form-control" name="profile_picture">
                            <br>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Address</label>
                            <textarea name="address" class="form-control" rows="2">{{$user->address}}</textarea>
                        </div>
                    </div>



                </div>


        </div>
        <!-- /.card-body -->
    </div>


        {{-- --------------------------------------------------Employee Information------------------------------------------------------------------- --}}

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Personal Details</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <!-- text input -->
                            <div class="form-group">
                                <label>Blood Group</label>
                                <input name="blood_group" type="text" class="form-control" value="{{optional($user->employee)->blood_group}}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>CNIC</label>
                                <input value="{{optional($user->employee)->cnic}}" name="cnic" type="text" pattern="\d*" minlength="13" maxlength="13" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <!-- text input -->
                            <div class="form-group">
                                <label>allergies</label>
                                <input value="{{optional($user->employee)->allergies}}" name="allergies" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Emergency Name</label>
                                <input value="{{optional($user->employee)->emergency_name}}" name="emergency_name" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <!-- textarea -->
                            <div class="form-group">
                                <label>Emergency Contact</label>
                                <input value="{{optional($user->employee)->emergency_contact}}" name="emergency_contact" type="text" pattern="\d*" minlength="11" maxlength="11" title=" " class="form-control">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <!-- textarea -->
                            <div class="form-group">
                                <label>Emergency Relationship</label>
                                <input value="{{optional($user->employee)->emergency_relationship}}" name="emergency_relationship" type="text" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>CNIC Address</label>
                                <textarea name="cnic_address" class="form-control" rows="2">{{optional($user->employee)->cnic_address}}</textarea>
                            </div>
                        </div>

                        {{-- <div class="col-sm-6">
                            <!-- textarea -->
                            <div class="form-group">
                                <label>CNIC Picture</label>
                                <input type="file" accept="image/*" class="form-control" name="cnic_picture">
                            </div>
                        </div> --}}


                    </div>

                    <div class="row">

                        <div class="col-sm-6">
                            <!-- textarea -->
                            <div class="form-group">
                                <label>Salary Slip Picture</label>
                                <input type="file" accept="image/*" class="form-control" name="salary_slip_pic">
                                <br>
                                <img style="width: 150px; height:150px;" src="{{asset('documents')}}/{{optional($user->employee)->salary_slip_pic}}" alt="">
                            </div>
                        </div>


                        <div class="col-sm-6">
                            <!-- textarea -->
                            <div class="form-group">
                                <label>CNIC Picture</label>
                                <input type="file" accept="image/*" class="form-control" name="cnic_pic">
                                <br>
                                <img style="width: 150px; height:150px;" src="{{asset('documents')}}/{{optional($user->employee)->cnic_pic}}" alt="">
                            </div>
                        </div>


                    </div>


            </div>
            <!-- /.card-body -->
        </div>


        {{-- --------------------------------------------------Bank Information------------------------------------------------------------------- --}}

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Bank Details</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <!-- text input -->
                            <div class="form-group">
                                <label>Bank Name</label>
                                <input name="bank_name" type="text" class="form-control" value="{{optional($user->bank)->bank_name}}">
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <!-- text input -->
                            <div class="form-group">
                                <label>Account Title</label>
                                <input name="account_title" type="text" class="form-control" value="{{optional($user->bank)->account_title}}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Account Number</label>
                                <input name="account_number" type="text" pattern="[0-9]{14,16}" class="form-control" value="{{optional($user->bank)->account_number}}">
                            </div>
                        </div>
                    </div>


                    <div class="text-right">
                        <input type="submit" value="Submit" class="btn btn-primary btn-lg">
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>

    @if (Session::has('message'))
        <script>
            $(document).ready(function() {
                Toast.fire({
                    icon: 'success',
                    title: 'User Created Successfully'
                })
            });
        </script>
    @endif

@endsection
