@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="{{ asset('reg_form/fonts/material-icon/css/material-design-iconic-font.min.css') }}">
    <link rel="stylesheet" href="{{ asset('reg_form/vendor/nouislider/nouislider.min.css') }}">

    <!-- Main css -->
    <link rel="stylesheet" href="{{ asset('reg_form/css/style2.css') }}">
    <style>
        .form-control {
            display: block;
            width: 100%;
            height: auto;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

    </style>
    <div class="main">
        @if (Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="">
            <div class="signup-content">
                {{-- <div class="signup-img">
                <img src="{{asset('reg_form/images/form-img.jpg')}}" alt="">
                <div class="signup-img-content">
                    <h2>Register now </h2>
                    <p>while seats are available !</p>
                </div>`
            </div> --}}
                <div class="signup-form">
                    <form class="register-form" id="register-form">
                        @csrf
                        <h4>Personal Information</h4>
                        <div class="form-row">
                            <div class="form-group">
                                <div class="form-input">
                                    <label for="first_name" class="required">First name</label>
                                    <input value="{{ auth()->user()->first_name }}" type="text" name="first_name"
                                        id="first_name" value="{{ auth()->user()->first_name }}" />
                                </div>
                                <div class="form-input">
                                    <label for="last_name" class="required">Contact</label>
                                    <input value="{{ auth()->user()->contact }}" type="text" name="contact" id="contact" />
                                </div>

                                <div class="form-input">
                                    <label for="last_name" class="required">Email</label>
                                    <input value="{{ auth()->user()->email }}" type="text" name="email" id="email" />
                                </div>

                                <div class="form-input">
                                    <label for="last_name" class="required">Blood Group</label>
                                    <input value="{{ auth()->user()->employee->blood_group }}" type="text"
                                        name="blood_group" id="blood_group" />
                                </div>



                                <div class="form-input">
                                    <label for="phone_number" class="required">CNIC Address</label>
                                    <textarea name="cnic_address" class="form-control" name="cnic_address" id="" cols="30"
                                        rows="6">{{ auth()->user()->employee->cnic_address }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-input">
                                    <label for="last_name" class="required">Last name</label>
                                    <input value="{{ auth()->user()->last_name }}" type="text" name="last_name"
                                        id="last_name" />
                                </div>


                                <div class="form-input">
                                    <label for="company" class="required">CNIC</label>
                                    <input value="{{ auth()->user()->employee->cnic }}" type="text" name="cnic"
                                        id="cnic" />
                                </div>

                                <div class="form-input">
                                    <label for="company" class="required">Password</label>
                                    <input value="{{ auth()->user()->password }}" type="password" name="password"
                                        id="password" />
                                </div>

                                <div class="form-input">
                                    <label for="company" class="required">Allergies</label>
                                    <input value="{{ auth()->user()->employee->allergies }}" type="text" name="allergies"
                                        id="allergies" />
                                </div>

                                <div class="form-input">
                                    <label for="payable">Current Address</label>
                                    <textarea name="address" class="form-control" name="address" id="" cols="30"
                                        rows="6">{{ auth()->user()->address }}</textarea>

                                </div>
                            </div>
                        </div>
                        <br>

                        {{-- -----------------------------------------------------------Bank Details Section---------------------------------------------------------------------- --}}
                        <h4>Bank Details</h4>
                        <div class="form-row">
                            <div class="form-group">
                                <div class="form-input">
                                    <label for="first_name" class="required">Bank Name</label>
                                    <input value="{{ auth()->user()->bank->bank_name }}" type="text" name="bank_name"
                                        id="bank_name" />
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="form-input">
                                    <label for="last_name" class="required">Account Title:</label>
                                    <input value="{{ auth()->user()->bank->account_title }}" type="text"
                                        name="account_title" id="account_title" />
                                </div>



                            </div>


                        </div>
                        <div class="form-input">
                            <label for="last_name" class="required">Account Number:</label>
                            <input value="{{ auth()->user()->bank->account_number }}" type="text" name="account_number"
                                id="account_number" />
                        </div>



                        {{-- -----------------------------------------------------------Emergency Information Section---------------------------------------------------------------------- --}}

                        <h4>Emergency Information</h4>
                        <div class="form-row">
                            <div class="form-group">
                                <div class="form-input">
                                    <label for="first_name" class="required">Name</label>
                                    <input value="{{ auth()->user()->employee->emergency_name }}" type="text"
                                        name="emergency_name" id="emergency_name" />
                                </div>
                                <div class="form-input">
                                    <label for="last_name" class="required">Relationship:</label>
                                    <input value="{{ auth()->user()->employee->emergency_relationship }}" type="text"
                                        name="emergency_relationship" id="emergency_relationship" />
                                </div>



                            </div>
                            <div class="form-group">
                                <div class="form-input">
                                    <label for="last_name" class="required">Contact</label>
                                    <input value="{{ auth()->user()->employee->emergency_contact }}" type="text"
                                        name="emergency_contact" id="emergency_contact" />
                                </div>


                                <div class="form-input">
                                    <label for="company" class="required">Email ID</label>
                                    <input value="{{ auth()->user()->employee->emergency_email }}" type="text"
                                        name="emergency_email" id="emergency_email" />
                                </div>

                            </div>
                        </div>



                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
