@if(auth()->user()->role_id==1)
<!-- need to remove -->
<style>
    [class*=sidebar-dark-] .nav-sidebar>.nav-item1.menu-open>.nav-link,
    [class*=sidebar-dark-] .nav-sidebar>.nav-item1>.nav-link:focus {
        background-color: #0b195261;
        color: #ffffff;
    }

</style>
    <li class="nav-item">
        <a href="{{ route('home') }}" class="nav-link @if(url()->current()==route('home')) active @endif">
            <i class="nav-icon fas fa-home"></i>
            <p>Home</p>
        </a>
    </li>

    <li class="nav-item">
        <a href="{{ route('users.index') }}" class="nav-link @if(url()->current()==route('users.index') || url()->current()==route('users.create')) active @endif">
            <i class="nav-icon fas fa-users"></i>
            <p>Users</p>
        </a>
    </li>


    {{-- <li class="nav-item has-treeview  @if(url()->current()==route('users.index') || url()->current()==route('users.create')) menu-open @endif">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-users"></i>
            <p>
                Users
                <i class="right fas fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{ route('users.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon @if(url()->current()==route('users.index')) far fa-dot-circle  red @endif" ></i>
                    <p>List</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('users.create') }}" class="nav-link">
                    <i class="far fa-circle nav-icon @if(url()->current()==route('users.create')) far fa-dot-circle  red @endif"></i>
                    <p>Create</p>
                </a>
            </li>

        </ul>
    </li> --}}

    {{-- <li class="nav-item has-treeview  @if(url()->current()==route('employees.index') || url()->current()==route('employees.create')) menu-open @endif">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-users"></i>
            <p>
                Employees
                <i class="right fas fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{ route('employees.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon @if(url()->current()==route('employees.index')) far fa-dot-circle  red @endif" ></i>
                    <p>List</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('employees.create') }}" class="nav-link">
                    <i class="far fa-circle nav-icon @if(url()->current()==route('employees.create')) far fa-dot-circle  red @endif"></i>
                    <p>Create</p>
                </a>
            </li>

        </ul>
    </li> --}}

@endif
