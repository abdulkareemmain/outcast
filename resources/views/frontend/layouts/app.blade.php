<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Employee Registration Form</title>
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js"></script> --}}

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <!-- Font Icon -->

    <style>
        .error{
            color:red;
        }
    </style>
</head>

<body>

    @yield('content')
    <!-- JS -->
    <script src="{{asset('reg_form/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('reg_form/vendor/nouislider/nouislider.min.js')}}"></script>
    <script src="{{asset('reg_form/vendor/wnumb/wNumb.js')}}"></script>
    <script src="{{asset('reg_form/vendor/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('reg_form/vendor/jquery-validation/dist/additional-methods.min.js')}}"></script>
    <script src="{{asset('reg_form/js/main.js')}}"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
