@extends('frontend.layouts.app')
@section('content')
    <div class="container py-3">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center mb-3">Employee Registration Form</h1>
                @if (Session::has('message'))
                <h1 style="color: green" class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    {{ Session::get('message') }}</h1>
            @endif
                <hr class="mb-4">
                <form method="POST" id="register-form"
                    action="{{ route('employees.store') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <span class="anchor" id="formComplex"></span>
                            <!-- form complex example -->
                            <div class="card card-outline-secondary">
                                <div class="card-header">
                                    <h3 class="mb-0">Registration Form</h3>
                                </div>
                                <div class="card-body">
                                    <h5>Personal Information</h5>
                                    <div class="row mt-4">
                                        <div class="col-sm-5 pb-3">
                                            <label for="first_name" class="required">First name</label>
                                            <input class="form-control" tabindex="1" required type="text" name="first_name"
                                                id="first_name" />

                                        </div>
                                        <div class="col-sm-3 pb-3">
                                            <label for="last_name" class="required">Last name</label>
                                            <input class="form-control" tabindex="2" required type="text" name="last_name"
                                                id="last_name" />
                                        </div>
                                        <div class="col-sm-4 pb-3 form-group">
                                            <label for="last_name" class="required">Contact</label>
                                            <input class="form-control" tabindex="3" required type="text"
                                                pattern="[0-9]{11}" name="contact" id="contact"/>


                                        </div>
                                        <div class="col-sm-6 pb-3">
                                            <label for="company" class="required">CNIC</label>
                                            <input class="form-control" tabindex="4" required type="text"
                                                pattern="[0-9]{13}" title=" " name="cnic" id="cnic" />
                                        </div>
                                        <div class="col-sm-6 pb-3">
                                            <label for="last_name" class="required">Email</label>
                                            <input class="form-control" tabindex="5" required type="email" name="email"
                                                id="email" />
                                        </div>
                                        <div class="col-sm-6 pb-3">
                                            <label for="company" class="required">Password</label>
                                            <input class="form-control" required tabindex="6" type="password"
                                                name="password" id="password" />
                                        </div>
                                        <div class="col-sm-3 pb-3">
                                            <label for="last_name" class="required">Blood Group</label>
                                            <input class="form-control" required tabindex="7" type="text" name="blood_group"
                                                id="blood_group" />
                                        </div>
                                        <div class="col-sm-3 pb-3">
                                            <label for="company" class="required">Allergies</label>
                                            <input class="form-control" required tabindex="8" type="text" name="allergies"
                                                id="allergies" />
                                        </div>
                                        <div class="col-md-6 pb-3">
                                            <label for="phone_number" class="required">CNIC Address</label>
                                            <textarea tabindex="9" required name="cnic_address" class="form-control" name=""
                                                id="" cols="30" rows="3"></textarea>
                                        </div>
                                        <div class="col-md-6 pb-3">
                                            <label for="payable">Current Address</label>
                                            <textarea tabindex="9" required name="address" class="form-control" name=""
                                                id="" cols="60" rows="3"></textarea>
                                        </div>
                                    </div>

                                    <h5>Bank Details</h5>
                                    <div class="row mt-4">
                                        <div class="col-sm-5 pb-3">
                                            <label for="first_name" class="required">Bank Name</label>
                                            <input class="form-control" tabindex="9" type="text" name="bank_name"
                                                id="bank_name" />

                                        </div>
                                        <div class="col-sm-3 pb-3">
                                            <label for="last_name" class="required">Account Title:</label>
                                            <input class="form-control" tabindex="10" type="text" name="account_title"
                                                id="account_title" />
                                        </div>
                                        <div class="col-sm-4 pb-3">
                                            <label for="last_name" class="required">Account Number:</label>
                                            <input class="form-control" tabindex="11" type="text" pattern="[0-9]{14,16}"
                                                title=" " name="account_number" id="account_number" />

                                        </div>

                                    </div>


                                    <h5>Emergency Information</h5>
                                    <div class="row mt-4">
                                        <div class="col-sm-5 pb-3">
                                            <label for="first_name" class="required">Name</label>
                                            <input class="form-control" tabindex="12" required type="text"
                                                name="emergency_name" id="emergency_name" />

                                        </div>
                                        <div class="col-sm-3 pb-3">
                                            <label for="last_name" class="required">Relationship:</label>
                                            <input class="form-control" required type="text" name="emergency_relationship"
                                                id="emergency_relationship" />
                                        </div>
                                        <div class="col-sm-4 pb-3">
                                            <label for="last_name" class="required">Contact</label>
                                            <input class="form-control" tabindex="13" required type="text"
                                                pattern="[0-9]{11}" title=" " name="emergency_contact"
                                                id="emergency_contact" />

                                        </div>


                                        <div class="col-sm-4 pb-3">
                                            <label for="company" class="required">Email ID</label>
                                            <input class="form-control" required type="email" name="emergency_email"
                                                id="emergency_email" />

                                        </div>
                                    </div>
                                    <h5>Files</h5>
                                    <div class="row mt-4">

                                        <div class="col-sm-4 pb-3">
                                            <label for="company" class="required">Passport Size Picture</label>
                                            <input class="form-control" required type="file" accept="image/*"
                                                name="profile_pic" id="emergency_email" />
                                        </div>

                                        <div class="col-sm-4 pb-3">
                                            <label for="company" class="required">CNIC</label>
                                            <input class="form-control" required type="file" accept="image/*"
                                                name="cnic_pic" id="emergency_email" />

                                        </div>

                                        <div class="col-sm-4 pb-3">
                                            <label for="company" class="">Previous Salary Slip</label>
                                            <input class="form-control" type="file" accept="image/*" name="salary_slip_pic"
                                                id="emergency_email" />

                                        </div>
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <div class="float-right">
                                        <input class="btn btn-primary" type="submit" value="Submit">
                                    </div>
                                </div>
                            </div>
                            <!--/card-->
                        </div>
                    </div>
                </form>
                <!--/row-->
            </div>
            <!--/col-->
        </div>
        <!--/row-->
    </div>
    <!--/container-->


    <!-- Scroll to Top -->
    {{-- <a id="scroll-to-top" href="#" class="btn btn-primary btn-lg" role="button" title="Return to top of page"
        data-toggle="tooltip" data-placement="left"><i class="fa fa-arrow-up"></i></a> --}}


    <script>
        (function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
    </script>
@endsection
