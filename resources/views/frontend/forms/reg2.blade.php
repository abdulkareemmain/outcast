@extends('frontend.layouts.app')
@section('content')
    <style>
        .form-control {
            display: block;
            width: 100%;
            height: auto;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

        .signup-img {
            background-color: black;
        }

        .signup-img img {
            width: 200px;
            /* height: 200px; */
            /* border-radius: 80%; */
            margin-left: 25%;
            margin-top: 144%;
        }

        textarea {
            resize: none;
        }

    </style>
    <div class="main">

        <div class="container">
            <div class="signup-content">
                <div class="signup-img">
                    <img src="https://outcastsolutions.us/web/web/public/img/logo/SUUGXq.png" alt="">
                    <div class="signup-img-content">
                        <h3 style="color: white;">Registration Form </h3>
                        {{-- <p>Register now !</p> --}}
                    </div>
                </div>
                <div class="signup-form">
                    <form method="POST" class="register-form" id="register-form" action="{{ route('employees.store') }}"
                        enctype="multipart/form-data">
                        @csrf
                        @if ($errors->any())
                            {!! implode('', $errors->all('<li style="color: red;">:message</li>')) !!}
                        @endif
                        @if (Session::has('message'))
                            <h1 style="color: red" class="alert {{ Session::get('alert-class', 'alert-info') }}">
                                {{ Session::get('message') }}</h1>
                        @endif
                        <h1>Personal Information</h1>
                        <div class="form-row">

                            {{-- ----------------------Col 1--------------------------------- --}}
                            <div class="form-group">
                                <div class="form-input">
                                    <label for="first_name" class="required">First name</label>
                                    <input tabindex="1" required type="text" name="first_name" id="first_name" />
                                </div>

                                <div class="form-input">
                                    <label for="last_name" class="required">Email</label>
                                    <input tabindex="5" required type="email" name="email" id="email" />
                                </div>

                                <div class="form-input" style="width: 240%">
                                    <label for="phone_number" class="required">CNIC Address</label>
                                    <textarea tabindex="9" required name="cnic_address" class="form-control" name="" id="" cols="30"
                                        rows="3"></textarea>
                                </div>



                            </div>


                            {{-- ----------------------Col 2--------------------------------- --}}

                            <div class="form-group">
                                <div class="form-input">
                                    <label for="last_name" class="required">Last name</label>
                                    <input tabindex="2" required type="text" name="last_name" id="last_name" />
                                </div>

                                <div class="form-input">
                                    <label for="company" class="required">Password</label>
                                    <input required tabindex="6" type="password" name="password" id="password" />
                                </div>




                            </div>

                            {{-- ----------------------Col 3--------------------------------- --}}

                            <div class="form-group">
                                <div class="form-input">
                                    <label for="last_name" class="required">Contact</label>
                                    <input tabindex="3" required type="number" pattern="[0-9]{11}" name="contact" id="contact" title=" " />
                                </div>
                                <script>
                                    function invalid(name){
                                        alert(name+'is invalid')
                                    }
                                </script>
                                <div class="form-input">
                                    <label for="last_name" class="required">Blood Group</label>
                                    <input required tabindex="7" type="text" name="blood_group" id="blood_group" />
                                </div>

                                <div class="form-input" style="width: 240%;">
                                    <label for="payable">Current Address</label>
                                    <textarea tabindex="9" required name="address" class="form-control" name="" id="" cols="60"
                                        rows="3"></textarea>

                                </div>

                            </div>

                            {{-- ----------------------Col 4--------------------------------- --}}

                            <div class="form-group">
                                <div class="form-input">
                                    <label for="company" class="required">CNIC</label>
                                    <input tabindex="4" required type="number" pattern="[0-9]{13}" title=" " name="cnic" id="cnic" />
                                </div>

                                <div class="form-input">
                                    <label for="company" class="required">Allergies</label>
                                    <input required tabindex="8" type="text" name="allergies" id="allergies" />
                                </div>

                            </div>


                        </div>
                        <br>

                        {{-- -----------------------------------------------------------Bank Details Section---------------------------------------------------------------------- --}}
                        <h1>Bank Details</h1>
                        <div class="form-row">
                            <div class="form-group">
                                <div class="form-input">
                                    <label for="first_name" class="required">Bank Name</label>
                                    <input tabindex="9" type="text" name="bank_name" id="bank_name" />
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="form-input">
                                    <label for="last_name" class="required">Account Title:</label>
                                    <input tabindex="10" type="text" name="account_title" id="account_title" />
                                </div>
                            </div>


                        </div>
                        <div class="form-input">
                            <label for="last_name" class="required">Account Number:</label>
                            <input tabindex="11" type="number" pattern="[0-9]{14,16}" title=" " name="account_number" id="account_number" />
                        </div>



                        {{-- -----------------------------------------------------------Emergency Information Section---------------------------------------------------------------------- --}}

                        <h1>Emergency Information</h1>
                        <div class="form-row">
                            <div class="form-group">
                                <div class="form-input">
                                    <label for="first_name" class="required">Name</label>
                                    <input tabindex="12" required type="text" name="emergency_name" id="emergency_name" />
                                </div>
                                <div class="form-input">
                                    <label for="last_name" class="required">Relationship:</label>
                                    <input required type="text" name="emergency_relationship" id="emergency_relationship" />
                                </div>



                            </div>
                            <div class="form-group">
                                <div class="form-input">
                                    <label for="last_name" class="required">Contact</label>
                                    <input tabindex="13" required type="number" pattern="[0-9]{11}" title=" " name="emergency_contact" id="emergency_contact" />
                                </div>


                                <div class="form-input">
                                    <label for="company" class="required">Email ID</label>
                                    <input required type="email" name="emergency_email" id="emergency_email" />
                                </div>

                            </div>
                        </div>

                        <div class="form-row">

                            <div class="form-group">
                                <div class="form-input">
                                    <label for="company" class="required">Passport Size Picture</label>
                                    <input required type="file" accept="image/*" name="profile_pic" id="emergency_email" />
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="form-input">
                                    <label for="company" class="required">CNIC</label>
                                    <input required type="file" accept="image/*" name="cnic_pic" id="emergency_email" />
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="form-input">
                                    <label for="company" class="">Previous Salary Slip</label>
                                    <input type="file" accept="image/*" name="salary_slip_pic" id="emergency_email" />
                                </div>
                            </div>

                        </div>


                        <div class="form-submit">
                            <input required type="submit" value="Submit" class="submit" id="submit" name="submit" />
                            <input required type="submit" value="Reset" class="submit" id="reset" name="reset" />
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
